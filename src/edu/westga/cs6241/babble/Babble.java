package edu.westga.cs6241.babble;
import edu.westga.cs6241.babble.views.BabbleGUI;


/**
 * Main class for starting a Babble game. 
 * @author lewisb
 * @version Fall 2016
 *
 */
public class Babble {
	
	/**
	 * Main method present in every application
	 * @param args array holding parameter
	 * @throws Exception for when something goes wrong
	 */
	public static void main(String[] args) throws Exception {
	    BabbleGUI gui = new BabbleGUI();
	    gui.setSize(gui.getPreferredSize());
	    gui.setVisible(true);
	}

}
