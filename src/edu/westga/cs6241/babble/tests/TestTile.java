package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;

/**
 * Test cases for the Tile class
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class TestTile {

	/**
	 * Basic test for simple constructor for Tile using getLetter method
	 */
	@Test
	public void testGetLetterA() {
		Tile testTile = new Tile('A', 1);
		assertEquals('A', testTile.getLetter());
	}
	
	/**
	 * Basic test for simple constructor for Tile using getPointValue method
	 */
	@Test
	public void testGetPointValue1() {
		Tile testTile = new Tile('A', 1);
		assertEquals(1, testTile.getPointValue());
	}
	
	/**
	 * Additional test for verification using different constructor data, will use Z, and 10
	 */
	@Test
	public void testGetLetterZ() {
		Tile testTile = new Tile('Z', 10);
		assertEquals('Z', testTile.getLetter());
	}
	
	/**
	 * Additional test for verification using different constructor data, will use Z, and 10
	 */
	@Test
	public void testGetPointValue10() {
		Tile testTile = new Tile('Z', 10);
		assertEquals(10, testTile.getPointValue());
	}
}
