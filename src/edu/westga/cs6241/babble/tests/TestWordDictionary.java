package edu.westga.cs6241.babble.tests;

import edu.westga.cs6241.babble.controllers.WordDictionary;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for WordDictionary class
 * @author Professor Baumstark
 * @version Fall 2016
 *
 */
public class TestWordDictionary {
	private WordDictionary dictionary;
	
	/**
	 * Setup method
	 * @throws Exception exception that will be thrown if error occurs
	 */
	@Before
	public void setUp() throws Exception {
		this.dictionary = new WordDictionary();
	}
	
	/**
	 * required class 
	 * @throws Exception exception that will be thrown if error occurs
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Test for when valid string
	 */
	@Test
	public void stringExpandShouldBeValid() {
		Assert.assertTrue(this.dictionary.isValidWord("Expand"));
	}

	/**
	 * Test for when invalid string Bugblat
	 */
	@Test
	public void stringBugblatShouldNotBeValid() {
		Assert.assertFalse(this.dictionary.isValidWord("Bugblat"));
	}

	/**
	 * Test for when invalid string is empty
	 */
	@Test
	public void emptyStringShouldNotBeValid() {
		Assert.assertFalse(this.dictionary.isValidWord(""));
	}
}
