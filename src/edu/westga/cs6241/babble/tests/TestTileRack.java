package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileRack;
import edu.westga.cs6241.babble.model.Word;

/**
 * Test cases for TileRack Class
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class TestTileRack {

	/**
	 * Sets up a basic test for a TileRack to make sure it's populated per the size given by the constructor
	 */
	@Test
	public void testTileRackConstructor() {
		TileRack testRack = new TileRack(7);
		assertEquals(0, testRack.getTiles().size());
	}
	
	/**
	 * Sets up a tile rack with some simple tiles, then uses the canMakeWordFrom method to check if the tiles are there to be used
	 */
	@Test
	public void testTileRackAppendMethodGetSize() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		testRack.append(testTile);
		assertEquals(1, testRack.getTiles().size());
	}
	
	/**
	 * Sets up a tile rack with some simple tiles, then uses the canMakeWordFrom method to check if the tiles are there to be used
	 */
	@Test
	public void testTileRackAppendMethodGetLetter() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		testRack.append(testTile);
		assertEquals('A', testRack.getTiles().get(0).getLetter());
	}
	
	/**
	 * Tests the inherited remove method in TileGroup
	 */
	@Test
	public void testTileRackRemoveMethodSimple() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		testRack.append(testTile);
		testRack.append(differentTile);
		testRack.remove(testRack.getTiles().get(0));
		assertEquals(1, testRack.getTiles().size());		
	}
	
	/**
	 * Tests the getNumberOfTiles needed method
	 */
	@Test
	public void testGetNumberOfTilesNeededIs5() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		testRack.append(testTile);
		testRack.append(differentTile);
		assertEquals(5, testRack.getNumberOfTilesNeeded());			
	}
	
	/**
	 * Tests the removeWord method with can
	 */
	@Test
	public void testCanMakeWordFromBAD() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('B', 1);
		Tile differentTile = new Tile('A', 2);
		Tile otherTile = new Tile('D', 2);
		testRack.append(testTile);		
		testRack.append(differentTile);
		testRack.append(otherTile);
		assertEquals(true, testRack.canMakeWordFrom("BAD"));
	}
	
	/**
	 * Tests the removeWord method with can
	 */
	@Test
	public void testCanMakeWordFromABD() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		Tile otherTile = new Tile('D', 2);
		testRack.append(testTile);		
		testRack.append(differentTile);
		testRack.append(otherTile);
		assertEquals(true, testRack.canMakeWordFrom("BAD"));
	}
	
	/**
	 * Tests the removeWord method with can
	 */
	@Test
	public void testCanMakeWordFromBADE() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		Tile otherTile = new Tile('D', 2);
		Tile finalTile = new Tile('E', 4);
		testRack.append(testTile);		
		testRack.append(differentTile);
		testRack.append(otherTile);
		testRack.append(finalTile);
		assertEquals(true, testRack.canMakeWordFrom("BAD"));
	}
	
	/**
	 * Tests the removeWord method with can
	 */
	@Test
	public void testCanNOTMakeWordFromBADE() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		Tile otherTile = new Tile('D', 2);
		Tile finalTile = new Tile('E', 4);
		testRack.append(testTile);		
		testRack.append(differentTile);
		testRack.append(otherTile);
		testRack.append(finalTile);
		assertEquals(false, testRack.canMakeWordFrom("CAD"));
	}
	
	/**
	 * Initial test case for RemoveWord method, will test if appropriate word is being return
	 */
	@Test
	public void testRemoveWordTestReturnBAD() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		Tile otherTile = new Tile('D', 2);
		Tile finalTile = new Tile('E', 4);
		testRack.append(testTile);		
		testRack.append(differentTile);
		testRack.append(otherTile);
		testRack.append(finalTile);
		Word testWord = testRack.removeWord("BAD");
		assertEquals("BAD", testWord.toString());		
	}
	
	/**
	 * Additional test case for RemoveWord method, will test if letters are being removed successfully
	 */
	@Test
	public void testRemoveWordTestRemainingTileIsE() {
		TileRack testRack = new TileRack(7);
		Tile testTile = new Tile('A', 1);
		Tile differentTile = new Tile('B', 2);
		Tile otherTile = new Tile('D', 2);
		Tile finalTile = new Tile('E', 4);
		testRack.append(testTile);		
		testRack.append(differentTile);
		testRack.append(otherTile);
		testRack.append(finalTile);
		testRack.removeWord("BAD");
		assertEquals("E", testRack.toString());		
	}	
	
	
}
