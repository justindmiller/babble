package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.Word;

/**
 * Test cases for Word class
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class TestWord {
	
	
	/**
	 * Test case for the score of the word
	 */
	@Test
	public void getScoreForWordRUN() {	
		Word testWord = new Word();
		testWord.append(new Tile('R', 1));
		testWord.append(new Tile('U', 1));
		testWord.append(new Tile('N', 1));
		assertEquals(3, testWord.getScore());
	}
	
	/**
	 * Test case for the score of the word
	 */
	@Test
	public void getScoreForWordZERO() {	
		Word testWord = new Word();
		testWord.append(new Tile('Z', 10));
		testWord.append(new Tile('E', 1));
		testWord.append(new Tile('R', 1));
		testWord.append(new Tile('O', 1));
		assertEquals(13, testWord.getScore());
	}
	
	/**
	 * Test case for the score of the word
	 */
	@Test
	public void getScoreForWordDECK() {	
		Word testWord = new Word();
		testWord.append(new Tile('D', 2));
		testWord.append(new Tile('E', 1));
		testWord.append(new Tile('C', 3));
		testWord.append(new Tile('K', 5));
		assertEquals(11, testWord.getScore());
	}
	
	/**
	 * Test case for the score of the word
	 */
	@Test
	public void getScoreForWordDEADLY() {	
		Word testWord = new Word();
		testWord.append(new Tile('D', 2));
		testWord.append(new Tile('E', 1));
		testWord.append(new Tile('A', 1));
		testWord.append(new Tile('D', 2));
		testWord.append(new Tile('L', 1));
		testWord.append(new Tile('Y', 4));
		assertEquals(11, testWord.getScore());
	}
	
	/**
	 * Tests the toString method inherited from TileGroup
	 */
	@Test
	public void getToStringForWordDEADLY() {
		Word testWord = new Word();
		testWord.append(new Tile('D', 2));
		testWord.append(new Tile('E', 1));
		testWord.append(new Tile('A', 1));
		testWord.append(new Tile('D', 2));
		testWord.append(new Tile('L', 1));
		testWord.append(new Tile('Y', 4));
		assertEquals("DEADLY", testWord.toString());		
	}
	
}
