package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileBag;

/**
 * Test cases for TileBag class
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class TestTileBag {


	
	/**
	 * Test for isEmpty method should return false, due to not enough drawn
	 */
	@Test
	public void testIsEmptyIsFalseNotEnoughDrawn() {
		TileBag emptyBag = new TileBag();
		for (int count = 90; count > 0; count--) {
			emptyBag.drawTile();
		}
		assertEquals(false, emptyBag.isEmpty());
	}
	
	/**
	 * Test for isEmpty method should return true, due to enough being drawn
	 */
	@Test
	public void testIsEmptyIsTrueEnoughDrawn() {
		TileBag emptyBag = new TileBag();
		for (int count = 98; count > 0; count--) {
			emptyBag.drawTile();
		}
		assertEquals(true, emptyBag.isEmpty());
	}
	
	/**
	 * Test to see if drawTile is populating an arraylist of tiles
	 */
	@Test 
	public void testIfDrawTileIsReturningTiles() {
		TileBag newBag = new TileBag();
		ArrayList<Tile> testTiles = new ArrayList<Tile>();
		testTiles.add(newBag.drawTile());
		assertEquals(1, testTiles.size());
	}
	
	/**
	 * Test for the size method in TileBag which is 98
	 */
	@Test
	public void testDefaultSizeOfTileBagShouldBe98() {
		TileBag newBag = new TileBag();
		int count = 0;
		while (!newBag.isEmpty()) {
			newBag.drawTile();
			count = count + 1;
		}
		assertEquals(98, count);
	}
	
	/**
	 * Test to see if drawTile is removing tiles as it is supposed too
	 */
	@Test
	public void testIfDrawTileIsRemovingFromBag() {
		TileBag newBag = new TileBag();
		ArrayList<Tile> testTiles = new ArrayList<Tile>();
		testTiles.add(newBag.drawTile());
		int count = 0;
		while (!newBag.isEmpty()) {
			newBag.drawTile();
			count = count + 1;
		}
		assertEquals(97, count);
	}
	
	/**
	 * Test to see if drawTile is returning proper tile object
	 */
	public void testDrawTileForProperReturn() {
		TileBag testBag = new TileBag();
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		tiles.add(testBag.drawTile());
		assertEquals(1, tiles.size());
	}
}
