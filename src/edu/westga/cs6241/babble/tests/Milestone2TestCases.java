package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.westga.cs6241.babble.model.EmptyTileBagException;
import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileBag;
import edu.westga.cs6241.babble.model.TileRack;
import edu.westga.cs6241.babble.model.Word;

/**
 * Test cases specified by Milestone 2
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class Milestone2TestCases {
	
	/**
	 * isEmpty should return true after drawing all tiles
	 */
	@Test
	public void canDrawAllTilesFromTileBag() {
		TileBag emptyBag = new TileBag();
		for (int count = 98; count > 0; count--) {
			emptyBag.drawTile();
		}
		assertEquals(true, emptyBag.isEmpty());
	}
	
	/**
	 * isEmpty should throw EmptyTileBagException
	 * @throws EmptyTileBagException exception that will be thrown if error occurs
	 */
	@Test(expected = EmptyTileBagException.class)
	public void cannotDraw100TilesFromTileBag() throws EmptyTileBagException {
		TileBag emptyBag = new TileBag();
		for (int count = 100; count > 0; count--) {
			try {
				emptyBag.drawTile();
			} catch (IllegalArgumentException e) {
				throw new EmptyTileBagException("Draw bag is already empty!");
			}			
		}
	}
	
	/**
	 * TileRack should contain word HEAVY
	 */
	@Test
	public void aTileRackShouldContainWordHeavy() {
		TileRack testRack = new TileRack(7);
		testRack.append(new Tile('H', 1));
		testRack.append(new Tile('E', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('V', 1));
		testRack.append(new Tile('Y', 1));
		testRack.append(new Tile('H', 1));
		testRack.append(new Tile('X', 1));
		assertEquals(true, testRack.canMakeWordFrom("HEAVY"));
	}
	
	/**
	 * TileRack should not contain word WET
	 */
	@Test
	public void aTileRackShouldNotContainWordWet() {
		TileRack testRack = new TileRack(7);
		testRack.append(new Tile('H', 1));
		testRack.append(new Tile('E', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('V', 1));
		testRack.append(new Tile('Y', 1));
		testRack.append(new Tile('H', 1));
		testRack.append(new Tile('X', 1));
		assertEquals(false, testRack.canMakeWordFrom("WET"));
	}
	
	/**
	 * TileRack should remove word HEAVY
	 */
	@Test
	public void aTileRackCanRemoveWordHeavy() {
		TileRack testRack = new TileRack(7);
		testRack.append(new Tile('H', 1));
		testRack.append(new Tile('E', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('V', 1));
		testRack.append(new Tile('Y', 1));
		testRack.append(new Tile('H', 1));
		testRack.append(new Tile('X', 1));
		testRack.removeWord("HEAVY");
		assertEquals(false, testRack.canMakeWordFrom("HEAVY"));
		assertEquals(5, testRack.getNumberOfTilesNeeded());
	}
	
	/**
	 * Empty tile rack should not have the word DWELL in it
	 */
	@Test
	public void aEmptyTileRackShouldNotHaveWordDwell() {
		TileRack testRack = new TileRack(7);
		assertEquals(false, testRack.canMakeWordFrom("DWELL"));
	}
	
	/**
	 * TileRack should not remove duplicate tiles
	 */
	@Test
	public void aTileRackShouldNotRemoveDuplicateTiles() {
		TileRack testRack = new TileRack(7);
		testRack.append(new Tile('C', 1));
		testRack.append(new Tile('A', 2));
		testRack.append(new Tile('T', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('A', 1));
		testRack.removeWord("CAT");
		assertEquals(true, testRack.canMakeWordFrom("AAAA"));
		assertEquals(3, testRack.getNumberOfTilesNeeded());
	}
	
	/**
	 * TileRack should remove word when the word is out of order
	 */
	@Test
	public void aTileRackCanRemoveWordWhenOutOfOrder() {
		TileRack testRack = new TileRack(7);
		testRack.append(new Tile('T', 1));		
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('C', 1));		
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('A', 1));
		testRack.append(new Tile('A', 1));
		testRack.removeWord("CAT");
		assertEquals(true, testRack.canMakeWordFrom("AAAA"));
		assertEquals(3, testRack.getNumberOfTilesNeeded());
	}
	
	/**
	 * Create 4 Point Tile F
	 */
	@Test
	public void createTileFwith4Points() {
		Tile testTile = new Tile('F', 4);
		assertEquals('F', testTile.getLetter());
		assertEquals(4, testTile.getPointValue());
	}
	
	/**
	 * Cannot create tile with letter h
	 */
	@Test(expected = IllegalArgumentException.class)
	public void cannotCreateTilewithh() {
		Tile testTile = new Tile('h', 4);
		assertEquals(4, testTile.getPointValue());
	}
	
	/**
	 * Cannot create tile with point value of -4
	 */
	@Test(expected = IllegalArgumentException.class)
	public void cannotCreateTilewithNegative4() {
		Tile testTile = new Tile('h', -4);
		assertEquals(-4, testTile.getPointValue());
	}

	/**
	 * Cannot create tile with point value of 0
	 */
	@Test(expected = IllegalArgumentException.class)
	public void cannotCreateTilewithZeroScore() {
		Tile testTile = new Tile('h', 0);
		assertEquals(0, testTile.getPointValue());
	}
	
	/**
	 * Create a word HEAVY with a score of 14
	 */
	@Test
	public void createWordHeavyWithScoreOf14() {
		Word testWord = new Word();
		testWord.append(new Tile('H', 4));
		testWord.append(new Tile('E', 1));
		testWord.append(new Tile('A', 1));
		testWord.append(new Tile('V', 4));
		testWord.append(new Tile('Y', 4));
		assertEquals(14, testWord.getScore());
	}
	
	/**
	 * Should show that an empty word has no score
	 */
	@Test
	public void emptyWordShouldBeZero() {
		Word testWord = new Word();
		assertEquals(0, testWord.getScore());
	}

}
