
package edu.westga.cs6241.babble.controllers;

import pt.tumba.spell.SpellChecker;

/**
 * Spell checker for words we attempt to make with Babble
 * 
 * @author lewisb
 * @version Fall 2016
 *
 */
public class WordDictionary {

	private SpellChecker checker;

	/**
	 * Creates a new WordDictionary
	 */
	public WordDictionary() {
		this.checker = new SpellChecker();

		try {
			this.checker.initialize("english.txt");
		} catch (Exception e) {
			System.err.println("Wrong path to dictionary file");
			e.printStackTrace();
		}
	}

	/**
	 * Determines if a word is a real word or not.
	 * 
	 * @param string
	 *            the word to check
	 * @return true if a valid word, false if gibberish
	 */
	public boolean isValidWord(String string) {
		String check = this.checker.spellCheckWord(string);
		return check.contains("<plain>");
	}
}
