package edu.westga.cs6241.babble.model;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a "Scrabble"-like bag of tiles. Use it to generate
 * an initial set of Scrabble tiles that decrease in number every time a player
 * "draws" from the bag
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class TileBag {
	
	private ArrayList<Tile> tileBag;
	private Random randomNumber;
	
	/**
	 * Constructor for TileBag
	 * @postcondition TileBag is populated and ready to be drawn from
	 */
	public TileBag() {
		this.tileBag = new ArrayList<Tile>();
		this.populateBag();
		this.randomNumber = new Random();
	}

	/**
	 * Draws a tile from the bag
	 * @return newTile the tile drawn from the bag
	 */
	public Tile drawTile() {
		int selection = this.randomNumber.nextInt(this.tileBag.size());
		Tile potentialTile = this.tileBag.get(selection);
		Tile drawnTile = new Tile(potentialTile.getLetter(), potentialTile.getPointValue());
		this.tileBag.remove(selection);
		return drawnTile;
	}
	
	
	/**
	 * Returns true if the TileBag object has no more tiles
	 * @return true if there are no more tiles
	 */
	public Boolean isEmpty() {
		return this.tileBag.isEmpty();
	}
	
	/**
	 * Populated the initial bag with the tiles and correct point values
	 */
	private void populateBag() {
		this.create1PointTiles();
		this.create2PointTiles();
		this.create3PointTiles();
		this.create4PointTiles();
		this.create5PointTiles();
		this.create8PointTiles();
		this.create10PointTiles();
	}
	
	private void create1PointTiles() {
		for (int count = 12; count > 0; count--) {
			this.tileBag.add(new Tile('E', 1));
		}
		for (int count = 9; count > 0; count--) {
			this.tileBag.add(new Tile('A', 1));
		}
		for (int count = 9; count > 0; count--) {
			this.tileBag.add(new Tile('I', 1));
		}
		for (int count = 8; count > 0; count--) {
			this.tileBag.add(new Tile('O', 1));
		}
		for (int count = 6; count > 0; count--) {
			this.tileBag.add(new Tile('N', 1));
		}
		for (int count = 6; count > 0; count--) {
			this.tileBag.add(new Tile('R', 1));
		}
		for (int count = 6; count > 0; count--) {
			this.tileBag.add(new Tile('T', 1));
		}
		for (int count = 4; count > 0; count--) {
			this.tileBag.add(new Tile('L', 1));
		}
		for (int count = 4; count > 0; count--) {
			this.tileBag.add(new Tile('S', 1));
		}
		for (int count = 4; count > 0; count--) {
			this.tileBag.add(new Tile('U', 1));
		}		
	}
	
	private void create2PointTiles() {
		for (int count = 4; count > 0; count--) {
			this.tileBag.add(new Tile('D', 2));
		}
		for (int count = 3; count > 0; count--) {
			this.tileBag.add(new Tile('G', 2));
		}

	}
	
	private void create3PointTiles() {
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('B', 3));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('C', 3));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('M', 3));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('P', 3));
		}	
	}
	
	private void create4PointTiles() {
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('F', 4));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('H', 4));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('V', 4));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('W', 4));
		}
		for (int count = 2; count > 0; count--) {
			this.tileBag.add(new Tile('Y', 4));
		}	
	}
	
	private void create5PointTiles() {
		this.tileBag.add(new Tile('K', 5));
	}
	
	private void create8PointTiles() {
		this.tileBag.add(new Tile('J', 8));	
		this.tileBag.add(new Tile('X', 8));
	}
	
	private void create10PointTiles() {
		this.tileBag.add(new Tile('Q', 10));	
		this.tileBag.add(new Tile('Z', 10));		
	}
	 
	
}
