package edu.westga.cs6241.babble.model;

/**
 * Exception for Empty Tile Bag
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class EmptyTileBagException extends IllegalArgumentException {

	private static final long serialVersionUID = 16848185;
	
	private String message;
	
	/**
	 * Constructor for EmptyTileBagException
	 * @param message message that will be displayed to user
	 */
	public EmptyTileBagException(String message) {
		if (message.isEmpty()) {
			throw new IllegalArgumentException("Must have error message");			
		}
		this.message = message;
	}
	
	/**
	 * Getter for the message
	 * @return this.message
	 */
	public String getMessage() {
		return this.message;
	}

}
