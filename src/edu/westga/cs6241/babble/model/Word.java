package edu.westga.cs6241.babble.model;

/**
 * Tiles removed from tile rack when the player thinks
 * they have a valid word for scoring purposes
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class Word extends TileGroup {
	
	/**
	 * Returns the total score of the TileGroup
	 * @return score
	 */
	public int getScore() {
		int score = 0;
		for (Tile tile : super.getTiles()) {
			score = score + tile.getPointValue();
		}
		return score;
	}
}
