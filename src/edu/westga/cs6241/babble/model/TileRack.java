package edu.westga.cs6241.babble.model;

import java.util.ArrayList;

/**
 * A rack of 7 tiles from which a player tries to make a usable word.
 * Everytime a word's worth of tiles are removed, it must be refreshed by drawing more tiles
 * from the tile bag
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class TileRack extends TileGroup {

	private int maxSize;
	
	/**
	 * Constructor for TileRack class
	 * @param maxSize the maximum size of the tileRack
	 * @postcondition object is initialized
	 */
	public TileRack(int maxSize) {
		if (maxSize <= 0) {
			throw new IllegalArgumentException("Max Size cannot be zero or less!");
		}
		this.maxSize = maxSize;
	}
	
	/**
	 * Removes a words worth of tiles
	 * @param text the text of the word to be removed
	 * @return the word to be removed
	 */
	public Word removeWord(String text) {
		if (text == null) {
			throw new IllegalArgumentException("Text to be removed cannot be null!");
		}
		char[] theLetters = text.toCharArray();
		Word scoreWord = new Word();
		for (char current : theLetters) {
			Tile currentTile = this.checkLetter(current);
			if (currentTile != null) {
				scoreWord.append(currentTile);
				super.remove(currentTile);
			}
		}		
		return scoreWord;
	}
	
	/**
	 * Returns the number of tiles needed
	 * @return tiles the tiles needed
	 */
	public int getNumberOfTilesNeeded() {		
		return this.maxSize - super.getTiles().size();
	}
	
	/**
	 * This method returns true if all the letters in text are available in TileRack
	 * @param text the text to be checked if a word can be made from
	 * @return true if letters are available
	 */
	public boolean canMakeWordFrom(String text) {
		if (text == null) {
			throw new IllegalArgumentException("Text to make word from cannot be null!");
		}
		ArrayList<Tile> tiles = super.getTiles();
		ArrayList<Character> tileLetters = new ArrayList<Character>();
		for (Tile tile : tiles) {
			tileLetters.add(tile.getLetter());
		}
		ArrayList<Character> textLetters = new ArrayList<Character>();
		char[] letters = text.toCharArray();
		for (char letter : letters) {
			textLetters.add(letter);
		}
		return tileLetters.containsAll(textLetters);
	}
	

	private Tile checkLetter(char letter) {
		for (Tile tile : super.getTiles()) {
			if (tile.getLetter() == letter) {
				return tile;
			}
		}
		return null;
	}
	
}
