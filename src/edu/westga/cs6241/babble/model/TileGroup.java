package edu.westga.cs6241.babble.model;

import java.util.ArrayList;

/**
 * Abstract class for manipulating collections of tiles
 * the toString() returns the letters of the tiles in order
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public abstract class TileGroup {
		
	private ArrayList<Tile> tiles;
	
	/**
	 * Constructor for TileGroup object
	 * @postcondition object is initialized
	 */
	public TileGroup() {
		this.tiles = new ArrayList<Tile>();
	}
	
	/**
	 * Adds a new tile
	 * @param tile the tile to be added
	 */
	public void append(Tile tile) {
		this.tiles.add(tile);
	}
	
	/**
	 * Removes a tile
	 * @param tile the tile to be removed
	 */
	public void remove(Tile tile) {
		this.tiles.remove(tile);
	}
	
	/**
	 * returns the letters of the tiles in order
	 * @return tileLetters string with the tiles in the correct order
	 */
	public String toString() {
		String tileLetters = "";
		for (Tile letter : this.tiles) {
			tileLetters = tileLetters + letter.getLetter();
		}
		return tileLetters;
	}
	
	/**
	 * Gets the tiles in the array list of Tiles
	 * @return this.tiles
	 */
	public ArrayList<Tile> getTiles() {
		return this.tiles;
	}
}
