package edu.westga.cs6241.babble.model;

/**
 * The tile object that contains the letter and numeric value of the tile
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class Tile {

	private char letter;
	private int pointValue;
	
	/**
	 * Constructor for Tile class, will hold a letter and point value
	 * @param letter the letter parameter
	 * @param pointValue the pointValue to be used
	 * @postcondition object is initialized
	 */
	public Tile(char letter, int pointValue) {
		if (Character.isLowerCase(letter)) {
			throw new IllegalArgumentException("Letter must be upper case");
		}
		if (pointValue <= 0) {
			throw new IllegalArgumentException("Point value cannot be zero or less");
		}
		this.letter = letter;
		this.pointValue = pointValue;
	}
	
	/**
	 * Returns the char contained in letter
	 * @return letter
	 */
	public char getLetter() {
		return this.letter;
	}
	
	/**
	 * Returns the point value contained within the tile
	 * @return pointValue
	 */
	public int getPointValue() {
		return this.pointValue;
	}
}
