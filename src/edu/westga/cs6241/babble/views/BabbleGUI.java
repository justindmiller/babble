package edu.westga.cs6241.babble.views;

import java.awt.BorderLayout;
import java.awt.Component;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import edu.westga.cs6241.babble.controllers.BabbleController;
import edu.westga.cs6241.babble.model.Tile;

/**
 * GUI class for the Babble program
 * 
 * @author Justin Miller
 * @version Fall 2016
 *
 */
public class BabbleGUI extends JFrame {
	private static final long serialVersionUID = -6948733938415380226L;
	private BabbleController gameController;
	private JButton[] buttonTiles;
	private JTextField wordDisplay;
	private BorderLayout layout;
	private JButton reset;
	private JButton play;
	private JLabel totalScore;
	private JMenuBar menuBar;

	/**
	 * Constructor for Babble GUI, will initialize the basic game and then create the the GUI display
	 */
	public BabbleGUI() {
		this.gameController = new BabbleController();
		this.gameController.startGame();
		this.gameController.refreshTileRack();
		this.initGui();
	}
	
	/**
	 * Populates the frame with appropriate objects
	 */
	private void initGui() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (UnsupportedLookAndFeelException e) {
		}	
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setTitle("Babble: It's Copyright Friendly Scrabble!");
		this.layout = new BorderLayout();
		this.layout.setHgap(20);
		this.layout.setVgap(20);
		this.setLayout(this.layout);
		this.createMenuBar();
		this.addTileBoxes();
		this.populateTiles();
		this.addWordBox();
		this.addControlButtons();
		this.addScorePanel();				
		super.pack();
	}
	
	/**
	 * Adds the tiles with the letters
	 */
	private void addTileBoxes() {
		JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.LINE_AXIS));
		this.add(tilePanel, BorderLayout.PAGE_START);
		this.buttonTiles = new JButton[7];
		for (int current = 0; current < this.buttonTiles.length; current++) {
			this.buttonTiles[current] = new JButton(":(");
			tilePanel.add(this.buttonTiles[current]);
		}
		for (int current = 0; current < this.buttonTiles.length; current++) {
			JButton button = this.buttonTiles[current];
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					JButton clickedButton = (JButton) event.getSource();
					char letter = clickedButton.getText().charAt(0);
					BabbleGUI.this.addLetter(letter);
					clickedButton.setEnabled(false);
				}
			});
		}
	}
	
	/**
	 * Adds the word box
	 */
	private void addWordBox() {
		JPanel wordPanel = new JPanel();
		wordPanel.setLayout(new BoxLayout(wordPanel, BoxLayout.LINE_AXIS));
		wordPanel.add(new JLabel("Your word:"));
		this.wordDisplay = new JTextField();
		wordPanel.add(this.wordDisplay);
		this.add(wordPanel, BorderLayout.CENTER);
		this.wordDisplay.setEditable(false);
	}
	
	/**
	 * Adds the Control buttons
	 */
	private void addControlButtons() {
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.PAGE_AXIS));
		this.add(controlPanel, BorderLayout.LINE_END);
		this.reset = new JButton("Reset");
        this.reset.setAlignmentX(Component.CENTER_ALIGNMENT);
		controlPanel.add(this.reset);
		this.play = new JButton("Play Word");
        this.play.setAlignmentX(Component.CENTER_ALIGNMENT);
		controlPanel.add(this.play);
		this.reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				BabbleGUI.this.performReset();
			}
		});
		this.play.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String selection = BabbleGUI.this.wordDisplay.getText();
				int statusCheck = BabbleGUI.this.gameController.checkCandidateWord(selection);
				if (statusCheck == 0) {
					BabbleGUI.this.gameController.removeWord(selection);
					BabbleGUI.this.gameController.refreshTileRack();
					BabbleGUI.this.totalScore.setText("Total Score: " + Integer.toString(BabbleGUI.this.gameController.getScore()));
					BabbleGUI.this.performReset();
					BabbleGUI.this.populateTiles();
				} else {
					JOptionPane.showMessageDialog(BabbleGUI.this, "Not a valid word");
				}
			}
		});
	}
	
	/**
	 * Adds the score panel
	 */
	private void addScorePanel() {
		JPanel scorePanel = new JPanel(new FlowLayout());
		this.add(scorePanel, BorderLayout.PAGE_END);
		this.totalScore = new JLabel();
		this.totalScore.setText("Total score: 0");
		scorePanel.add(this.totalScore);		
	}
	
	/**
	 * Populate the tiles with characters matching the game state.
	 */
	private void populateTiles() {
		Tile[] theTiles = this.gameController.getTiles();
		for (int current = 0; current < theTiles.length; current++) {
			Tile tile = theTiles[current];
			char letter = tile.getLetter();
			int value = tile.getPointValue();
			this.buttonTiles[current].setText(letter + " (" + value + ")");
		}
	}
	
	/**
	 * Helper method to add a letter to the wordDisplay
	 * @param letter the letter to be added
	 */
	private void addLetter(char letter) {
		String currentWord = this.wordDisplay.getText();
		currentWord = currentWord + letter;
		this.wordDisplay.setText(currentWord);
	}
	
	/**
	 * Helper method execute the reset buttons features
	 */
	private void performReset() {
		for (int current = 0; current < this.buttonTiles.length; current++) {
			JButton button = this.buttonTiles[current];
			button.setEnabled(true);
		}
		this.wordDisplay.setText("");
	}
	
	/**
	 * Creates a menu bar for the program application with a File and Settings tab
	 */
	private void createMenuBar() {
		this.menuBar = new JMenuBar();
		this.createFileBar();      
        this.setJMenuBar(this.menuBar);		
	}
	
	/**
	 * Creates the file bar that is commonly found in windows applications
	 */
	private void createFileBar() {
        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);
        JMenuItem restart = new JMenuItem("Start New Game");
        restart.setMnemonic(KeyEvent.VK_N);
        restart.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.ALT_MASK));
        restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent action) {
                BabbleGUI.this.restart();
            }
        });
        file.add(restart);
        JMenuItem exit = new JMenuItem("Exit");
        exit.setMnemonic(KeyEvent.VK_X);
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK));
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent action) {
                System.exit(0);
            }
        });
        file.add(exit);
        this.menuBar.add(file);
	}
	
	/**
	 * Helper method to restart the game
	 */
	private void restart() {
		this.gameController.reset();
		this.totalScore.setText("Total score: 0");
		this.populateTiles();
	}

	
}
